USE [DMOperations]
GO

INSERT INTO dbo.[ChildPackageDetail]
(
    [ChildPackageDetailId],
    [FrameworkPackage_FrameworkOrchestrationRunId],
    [PackageSizeId],
    [PackageOperationalStatusId],
    [EnabledFlag],
    [LockNum],
    [PrecedenceLevel]
)
VALUES
(   -1001,    -- ChildPackageDetailId - int
    7445,    -- FrameworkPackage_FrameworkOrchestrationRunId - int
    0,    -- PackageSizeId - int
    1,    -- PackageOperationalStatusId - int
    1,    -- EnabledFlag - bit
    0,    -- LockNum - int
    2     -- PrecedenceLevel - int
),
(   -1002,    -- ChildPackageDetailId - int
    7446,    -- FrameworkPackage_FrameworkOrchestrationRunId - int
    0,    -- PackageSizeId - int
    1,    -- PackageOperationalStatusId - int
    1,    -- EnabledFlag - bit
    0,    -- LockNum - int
    2     -- PrecedenceLevel - int
),
(   -1003,    -- ChildPackageDetailId - int
    7447,    -- FrameworkPackage_FrameworkOrchestrationRunId - int
    0,    -- PackageSizeId - int
    1,    -- PackageOperationalStatusId - int
    1,    -- EnabledFlag - bit
    0,    -- LockNum - int
    2     -- PrecedenceLevel - int
),
(   -1004,    -- ChildPackageDetailId - int
    7448,    -- FrameworkPackage_FrameworkOrchestrationRunId - int
    0,    -- PackageSizeId - int
    1,    -- PackageOperationalStatusId - int
    1,    -- EnabledFlag - bit
    0,    -- LockNum - int
    2     -- PrecedenceLevel - int
)




SELECT 
            * 

FROM        dbo.[ChildPackageDetail] cpd
INNER JOIN  dbo.FrameworkPackage_FrameworkOrchestrationRun foid ON foid.FrameworkPackage_FrameworkOrchestrationRunId = cpd.FrameworkPackage_FrameworkOrchestrationRunId
INNER JOIN  dbo.FrameworkPackage fp ON fp.FrameworkPackageId = foid.FrameworkPackageId
WHERE 

fp.PackageNm IN (
                        'Package1',
                        'Package2',
                        'Package3',
                        'Package4'

                    -- 'REF_DIP_ReferenceDataAlternateCd_SRD OB001'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB002'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB003'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB004'
)

ORDER BY    cpd.ChildPackageDetailId DESC
