USE [DMOperations]
GO

INSERT INTO dbo.FrameworkOrchestrationRun
(
    FrameworkOrchestrationRunId,
    FrameworkOrchestrationRunCd,
    FrameworkOrchestrationRunNm,
    FrameworkOrchestrationRunDsc,
    InformationSourceId,
    SystemCreatedDt,
    SystemUpdatedDt,
    UpdateByStaffId
    --StartDate,
    --EndDate
)
VALUES
(   -1000,             -- FrameworkOrchestrationRunId - int
    'XPK_LogError_Test',            -- FrameworkOrchestrationRunCd - varchar(50)
    'XPK_LogError_Test',            -- FrameworkOrchestrationRunNm - varchar(200)
    'XPK_LogError_Test',            -- FrameworkOrchestrationRunDsc - varchar(2000)
    1,             -- InformationSourceId - int
    GETDATE(),     -- SystemCreatedDt - datetime
    GETDATE(),     -- SystemUpdatedDt - datetime
    0             -- UpdateByStaffId - int
    --SYSDATETIME(), -- StartDate - datetime2(7)
    --SYSDATETIME()  -- EndDate - datetime2(7)
)

SELECT * FROM FrameworkOrchestrationRun WHERE FrameworkOrchestrationRunNm = 'XPK_LogError_Test'
