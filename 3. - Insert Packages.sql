USE [DMOperations]
GO


--SET IDENTITY_INSERT dbo.FrameworkPackage ON

INSERT INTO dbo.FrameworkPackage
(
    FrameworkPackageId,
    PackageNm,
    BusinessTerm,
    BusinessDsc,
    PackageSequenceNumr,
    BAUpackageInd,
    FrameworkMonitoringSeverityId,
    FrameworkSourceProductId,
    FrameworkProjectId,
    FrameworkUsageID
)

VALUES
( -1001, 'Package1', NULL, NULL, NULL, '1', 18, 3, 1000, NULL ),
( -1002, 'Package2', NULL, NULL, NULL, '1', 18, 3, 1000, NULL ),
( -1003, 'Package3', NULL, NULL, NULL, '1', 18, 3, 1000, NULL ),
( -1004, 'Package4', NULL, NULL, NULL, '1', 18, 3, 1000, NULL )

SELECT * FROM dbo.[FrameworkPackage] WHERE PackageNm IN (
                                                         'Package1',
                                                         'Package2',
                                                         'Package3',
                                                         'Package4'
                                                        )