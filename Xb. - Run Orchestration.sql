USE [DMOperations]
GO

    DECLARE @ErrMsg1                  NVARCHAR(MAX),
            @ErrMsg2                  NVARCHAR(MAX),
            @ErrMsg                   NVARCHAR(MAX),
            @ErrorProcedure           sysname,
            @ErrorNumber              INT,
            @ErrorSeverity            INT,
            @ErrorState               INT,
            @ErrorLine                INT,
            @ErrorMessage             NVARCHAR(MAX),
            @Status                   INT,
            @ExecutionStatus          VARCHAR(50),
            @OrchestrationVariable AS OrchestrationVariable;

    SET @ErrMsg2 = '';
    SET @Status = -1;

        ----EXECUTE ORCHESTRATION Deposits-----------------------------------------------------------------------------
        DELETE FROM @OrchestrationVariable;
        INSERT INTO @OrchestrationVariable (VariableName,
                                            VariableValue,
                                            Sensitive)
        VALUES ('PKV_FrameworkOrchestrationRunId', '-1000', 0),
        ('PKV_SourceSystemId', '1000', 0),
        ('gv_CurrReqId', '0', 0),
        ('PKV_StartStatus', 'Prepared', 0),
        ('PKV_EndStatusPass', 'Received', 0),
        ('PKV_EndStatusFail', 'Completed with errors', 0);

        EXEC @Status = spExecutePackage @Package_Name = 'Orchestration',
                                        @OrchestrationVariable = @OrchestrationVariable;
        SELECT @Status