USE [AuditAndControl]
GO

INSERT INTO dbo.[SourceSystem]
(
    [SourceSystemId],
    [SourceSystemCd],
    [SourceSystemNm],
    [SourceSystemDsc],
    [InformationSourceId],
    [SystemCreatedDt],
    [UpdateByStaffId],
    [SystemUpdatedDt]
)
VALUES
(   1000,         -- SourceSystemId - int
    'XPK_LogError_Test',        -- SourceSystemCd - varchar(200)
    'XPK_LogError_Test',        -- SourceSystemNm - varchar(200)
    'XPK_LogError_Test',        -- SourceSystemDsc - varchar(2000)
    1,         -- InformationSourceId - int
    GETDATE(), -- SystemCreatedDt - datetime
    0,         -- UpdateByStaffId - int
    GETDATE()  -- SystemUpdatedDt - datetime
)

SELECT * FROM dbo.[SourceSystem] WHERE SourceSystemNm = 'XPK_LogError_Test'