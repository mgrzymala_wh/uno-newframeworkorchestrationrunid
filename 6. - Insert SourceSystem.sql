USE [DMOperations]
GO

INSERT INTO [dbo].[SourceSystem]
(
    [SourceSystemId],
    [SourceSystemCd],
    [SourceSystemNm],
    [SourceSystemDsc],
    [InformationSourceId],
    [SystemCreatedDt],
    [UpdateByStaffId],
    [SystemUpdatedDt]
)

VALUES
( 1000, 'XPK_LogError_Test', 'XPK_LogError_Test', 'XPK_LogError_Test', 1, GETDATE(), 1, GETDATE() )

SELECT * FROM [dbo].[SourceSystem] WHERE SourceSystemNm = 'XPK_LogError_Test'
