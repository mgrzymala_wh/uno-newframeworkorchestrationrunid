USE [DMOperations]
GO

-- 9: remove [ChildPackageDetail]
DELETE      cpd
FROM        dbo.[ChildPackageDetail] cpd
INNER JOIN  dbo.FrameworkPackage_FrameworkOrchestrationRun foid ON foid.FrameworkPackage_FrameworkOrchestrationRunId = cpd.FrameworkPackage_FrameworkOrchestrationRunId
INNER JOIN  dbo.FrameworkPackage fp ON fp.FrameworkPackageId = foid.FrameworkPackageId
WHERE 
fp.PackageNm IN (
                        'Package1',
                        'Package2',
                        'Package3',
                        'Package4'
)


USE [AuditAndControl]
GO

-- 8: remove [DataExportRequest]
DELETE der
FROM dbo.[DataExportRequest] der 
INNER JOIN dbo.SourceSystem ss ON ss.SourceSystemId = der.SourceSystemId 
WHERE ss.SourceSystemNm = 'XPK_LogError_Test'

-- 7: remove [SourceSystem] WHERE SourceSystemNm = 'XPK_LogError_Test'
DELETE FROM dbo.[SourceSystem] WHERE SourceSystemNm = 'XPK_LogError_Test'
GO


USE [DMOperations]
GO
-- 6: remove [SourceSystem]
DELETE FROM [dbo].[SourceSystem] WHERE SourceSystemNm = 'XPK_LogError_Test'

-- 5: -- remove [FrameworkPackage_FrameworkOrchestrationRun]:
--SELECT * 
DELETE frr
FROM [dbo].[FrameworkPackage_FrameworkOrchestrationRun] frr 
INNER JOIN dbo.FrameworkPackage fp ON fp.FrameworkPackageId = frr.FrameworkPackageId
WHERE fp.PackageNm IN (
                        'Package1',
                        'Package2',
                        'Package3',
                        'Package4'
)

-- 4: -- remove [FrameworkOrchestrationRun]:
DELETE FROM dbo.[FrameworkOrchestrationRun] WHERE FrameworkOrchestrationRunNm = 'XPK_LogError_Test'

-- 3: -- remove [FrameworkPackage]
DELETE FROM dbo.[FrameworkPackage] WHERE PackageNm IN (
                                                      'Package1',
                                                      'Package2',
                                                      'Package3',
                                                      'Package4'
                                                     )

-- 2: -- remove [FrameworkProject]:
DELETE FROM dbo.[FrameworkProject] WHERE FrameworkProjectNm = 'XPK_LogError_Test'

-- 1: -- remove [FrameworkFolder]:
DELETE FROM dbo.[FrameworkFolder] WHERE FrameworkFolderNm = 'XPK_LogError_Test'



