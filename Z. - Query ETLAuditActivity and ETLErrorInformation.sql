USE [DMOperations]
GO

SELECT 
            * 
FROM        dbo.ETLAuditActivity aa 
INNER JOIN  dbo.FrameworkPackage fp ON fp.FrameworkPackageId = aa.FrameworkPackageId
WHERE       fp.PackageNm IN (
                                'Package1',
                                'Package2',
                                'Package3',
                                'Package4'
)
ORDER BY aa.AuditEventInsertDt DESC 

SELECT 
            * 
FROM        dbo.ETLErrorInformation ei 
INNER JOIN  dbo.FrameworkPackage fp ON fp.FrameworkPackageId = ei.FrameworkPackageId
WHERE       fp.PackageNm IN (
                                'Package1',
                                'Package2',
                                'Package3',
                                'Package4'
)
ORDER BY ei.ErrorInformationInsertDt DESC 
