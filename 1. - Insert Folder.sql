USE [DMOperations]
GO

INSERT INTO [dbo].[FrameworkFolder]
(
    [FrameworkFolderId],
    [FrameworkFolderCd],
    [FrameworkFolderNm],
    [FrameworkFolderDsc],
    [InformationSourceId],
    [SystemCreatedDt],
    [SystemUpdatedDt],
    [UpdateByStaffId]
--    [StartDate],
--    [EndDate]
)


VALUES
( -1000, 'XPK_LogError_Test', 'XPK_LogError_Test', 'This folder holds all XPK_LogError_Test packages - DANSDPSRE-481', 1, GETDATE(), GETDATE(), 3
--,N'1900-01-01T00:00:00', N'9999-12-31T23:59:59.9999999' 
)

SELECT * FROM [dbo].[FrameworkFolder] WHERE FrameworkFolderNm = 'XPK_LogError_Test'