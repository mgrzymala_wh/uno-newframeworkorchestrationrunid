USE [AuditAndControl]
GO

INSERT INTO dbo.[DataExportRequest]
(
    [DataExportRequestId],
    [SourceSystemId],
    [TypeDsc],
    [ProcessedDt],
    [DataExportStatusDt],
    [DataExportTransferTypeId],
    [DataExportTypeId],
    [DataExportStatusId]
)
VALUES
(   1000,         -- DataExportRequestId - int
    1000,         -- SourceSystemId - int
    N'Periodic Load',       -- TypeDsc - nvarchar(100)
    GETDATE(), -- ProcessedDt - datetime
    GETDATE(), -- DataExportStatusDt - datetime
    57747,         -- DataExportTransferTypeId - int
    57165,         -- DataExportTypeId - int
    57181          -- DataExportStatusId - int 57181 Received
)


SELECT * FROM dbo.[DataExportRequest] der INNER JOIN dbo.SourceSystem ss ON ss.SourceSystemId = der.SourceSystemId WHERE ss.SourceSystemNm = 'XPK_LogError_Test'