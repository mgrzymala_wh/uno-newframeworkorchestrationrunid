DECLARE @execution_id BIGINT
EXEC [SSISDB].[catalog].[create_execution] @package_name=N'master.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'XPK_LogError_Test', @project_name=N'XPK_LogError_Test', @use32bitruntime=False, @reference_id=NULL
SELECT @execution_id
DECLARE @var0 SMALLINT = 1
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDB].[catalog].[start_execution] @execution_id
GO


