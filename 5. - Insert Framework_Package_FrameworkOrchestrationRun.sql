USE [DMOperations]
GO

INSERT INTO [dbo].[FrameworkPackage_FrameworkOrchestrationRun]
(
    [FrameworkOrchestrationRunId],
    [FrameworkPackageId],
    [FrameworkPackage_FrameworkOrchestrationRunCd],
    [FrameworkPackage_FrameworkOrchestrationRunNm],
    [FrameworkPackage_FrameworkOrchestrationRunsc],
    [InformationSourceId],
    [SystemCreatedDt],
    [SystemUpdatedDt],
    [UpdateByStaffId]
)
VALUES
(   -1000,         -- FrameworkOrchestrationRunId - int
    -1001,         -- FrameworkPackageId - int
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunCd - varchar(50)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunNm - varchar(200)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunsc - varchar(2000)
    0,         -- InformationSourceId - int
    GETDATE(), -- SystemCreatedDt - datetime
    GETDATE(), -- SystemUpdatedDt - datetime
    0          -- UpdateByStaffId - int
),

(   -1000,         -- FrameworkOrchestrationRunId - int
    -1002,         -- FrameworkPackageId - int
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunCd - varchar(50)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunNm - varchar(200)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunsc - varchar(2000)
    0,         -- InformationSourceId - int
    GETDATE(), -- SystemCreatedDt - datetime
    GETDATE(), -- SystemUpdatedDt - datetime
    0          -- UpdateByStaffId - int
),

(   -1000,         -- FrameworkOrchestrationRunId - int
    -1003,         -- FrameworkPackageId - int
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunCd - varchar(50)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunNm - varchar(200)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunsc - varchar(2000)
    0,         -- InformationSourceId - int
    GETDATE(), -- SystemCreatedDt - datetime
    GETDATE(), -- SystemUpdatedDt - datetime
    0          -- UpdateByStaffId - int
),

(   -1000,         -- FrameworkOrchestrationRunId - int
    -1004,         -- FrameworkPackageId - int
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunCd - varchar(50)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunNm - varchar(200)
    'XPK_LogError_Test',        -- FrameworkPackage_FrameworkOrchestrationRunsc - varchar(2000)
    0,         -- InformationSourceId - int
    GETDATE(), -- SystemCreatedDt - datetime
    GETDATE(), -- SystemUpdatedDt - datetime
    0          -- UpdateByStaffId - int
)


SELECT 
            foid.FrameworkPackage_FrameworkOrchestrationRunId,
            foid.FrameworkOrchestrationRunId,
            foid.FrameworkPackageId,
            foid.FrameworkPackage_FrameworkOrchestrationRunCd,
            foid.FrameworkPackage_FrameworkOrchestrationRunNm,
            foid.FrameworkPackage_FrameworkOrchestrationRunsc,
            foid.InformationSourceId,
            foid.SystemCreatedDt,
            foid.SystemUpdatedDt,
            foid.UpdateByStaffId
            
FROM        [dbo].[FrameworkPackage_FrameworkOrchestrationRun] foid 
INNER JOIN  [dbo].FrameworkPackage fp ON fp.FrameworkPackageId = foid.FrameworkPackageId
WHERE 

fp.PackageNm IN (
                        'Package1',
                        'Package2',
                        'Package3',
                        'Package4'

                    -- 'REF_DIP_ReferenceDataAlternateCd_SRD OB001'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB002'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB003'
                    --,'REF_DIP_ReferenceDataAlternateCd_SRD OB004'
)